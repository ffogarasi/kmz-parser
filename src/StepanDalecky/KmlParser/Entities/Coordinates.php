<?php
declare(strict_types=1);

namespace StepanDalecky\KmlParser\Entities;


class Coordinates extends Entity
{
    protected $points = [];


	public function getValue(): string
	{
		return $this->element->getValue();
	}
	
    private function parseCordinates($value) {
        $values = explode("\n", $value);
        
        foreach($values as $value) {
            array_push($this->points, Point::fromString(trim($value)));
        }
	}

	public function getPoints() {
        if (empty($this->points)) {
            $this->parseCordinates(trim($this->getValue()));
        }
        return $this->points;
    }
}
