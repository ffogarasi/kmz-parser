<?php
declare(strict_types=1);

namespace StepanDalecky\KmlParser\Entities;

class OuterBoundary extends Entity
{

	public function getOuterBoundary(): object
	{
		return $this->element->getAttribute('outerBoundaryIs');
	}

	public function getValue(): string
	{
		return $this->element->getValue();
	}

	public function getLinearRing(): LinearRing
	{
		return new LinearRing($this->element->getChild('LinearRing'));
	}

}
