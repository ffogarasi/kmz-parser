<?php
declare(strict_types=1);

namespace StepanDalecky\KmlParser\Entities;

class Polygon extends Entity
{

	public function getPolygon()
	{
		return $this->element->getAttribute('Polygon');
	}

	public function getValue(): string
	{
		return $this->element->getValue();
	}

	public function getOuterBoundary(): OuterBoundary
	{
		return new OuterBoundary($this->element->getChild('outerBoundaryIs'));
	}
}
