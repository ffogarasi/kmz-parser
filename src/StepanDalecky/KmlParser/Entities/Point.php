<?php
declare(strict_types = 1);

namespace StepanDalecky\KmlParser\Entities;

use StepanDalecky\KmlParser\XmlElement\Element;


class Point extends Entity
{
	public function __construct(Element $element)
	{
        $values = self::parseCordinate($element->getChild('coordinates')->getValue());
        
        $xmlElement = $element->getXmlElement();
        
        if (!$element->hasChild('lon')) {
            $xmlElement->addChild('lon', $values[0]);
        }
            
        if (!$element->hasChild('lat')) {
            $xmlElement->addChild('lat', $values[1]);
        }

        if (!$element->hasChild('alt')) {
            $xmlElement->addChild('alt', $values[2]);
        }
	
		parent::__construct($element);
	}

	public static function fromString($value) {
        $values = self::parseCordinate($value);
        
        $element = new \SimpleXMLElement("<Point/>");
        $element->addChild('coordinates', "{$values[0]},{$values[1]},{$values[2]}");
    
        return new self(new Element($element));
        }
    

	private static function parseCordinate($value) {
        $values = array_pad(explode(',', trim($value)), 3, 0);
        return $values;
	}
	
	public function getLatitude() {
        return $this->element->getChild('lat')->getValue();
	}
	
	public function getLongitude() {
        return $this->element->getChild('lon')->getValue();
	}
	
	public function getAltitude() {
        return $this->element->getChild('alt')->getValue();
	}
}
