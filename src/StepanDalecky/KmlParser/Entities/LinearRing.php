<?php
declare(strict_types=1);

namespace StepanDalecky\KmlParser\Entities;

class LinearRing extends Entity
{

	public function getLinearRing()
	{
		return $this->element->getAttribute('LinearRing');
	}

	public function getValue(): string
	{
		return $this->element->getValue();
	}

	public function getCoordinates(): Coordinates
	{
		return new Coordinates($this->element->getChild('coordinates'));
	}
}
