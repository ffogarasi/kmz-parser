<?php
declare(strict_types = 1);

namespace namespace StepanDalecky\KmlParser\XmlElement\Exceptions;

class UnexpectedXmlStructureException extends \OutOfBoundsException
{
}
